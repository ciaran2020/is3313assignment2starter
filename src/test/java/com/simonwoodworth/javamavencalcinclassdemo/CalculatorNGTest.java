/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simonwoodworth.javamavencalcinclassdemo;

import static org.testng.Assert.*;

/**
 *
 * @author User
 */
public class CalculatorNGTest {
    
    public CalculatorNGTest() {
    }

    @org.testng.annotations.BeforeClass
    public static void setUpClass() throws Exception {
    }

    @org.testng.annotations.AfterClass
    public static void tearDownClass() throws Exception {
    }

    @org.testng.annotations.BeforeMethod
    public void setUpMethod() throws Exception {
    }

    @org.testng.annotations.AfterMethod
    public void tearDownMethod() throws Exception {
    }

    /**
     * Test of add method, of class Calculator.
     */
    @org.testng.annotations.Test
    public void testAdd() {
    }

    /**
     * Test of subtract method, of class Calculator.
     */
    @org.testng.annotations.Test
    public void testSubtract() {
    }

    /**
     * Test of multiply method, of class Calculator.
     */
    @org.testng.annotations.Test
    public void testMultiply() {
    }

    /**
     * Test of divide method, of class Calculator.
     */
    @org.testng.annotations.Test
    public void testDivide() {
    }

    /**
     * Test of square method, of class Calculator.
     */
    @org.testng.annotations.Test
    public void testSquare() {
    }

    /**
     * Test of cube method, of class Calculator.
     */
    @org.testng.annotations.Test
    public void testCube() {
    }

    /**
     * Test of modulo method, of class Calculator.
     */
    @org.testng.annotations.Test
    public void testModulo() {
    }

    /**
     * Test of addThree method, of class Calculator.
     */
    @org.testng.annotations.Test
    public void testAddThree() {
    }

    /**
     * Test of subtractThree method, of class Calculator.
     */
    @org.testng.annotations.Test
    public void testSubtractThree() {
    }

    /**
     * Test of multiplyThree method, of class Calculator.
     */
    @org.testng.annotations.Test
    public void testMultiplyThree() {
    }

    /**
     * Test of divideThree method, of class Calculator.
     */
    @org.testng.annotations.Test
    public void testDivideThree() {
    }
    
}
